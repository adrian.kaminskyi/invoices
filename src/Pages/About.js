import React, { Component } from 'react'
import Create from '../Components/Create'
import Header from '../Components/Header'

export default class About extends Component {
    render() {
        return (
            <div>
                <Header pageName='Create Invoice' />
                <Create />
            </div>
        )
    }
}
