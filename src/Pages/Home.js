import React, { Component } from 'react'
import Header from '../Components/Header'
import Action from '../Components/Action'
import Invoices from '../Components/Invoices'

export default class Home extends Component {
    render() {
        return (
            <div>
                <Header pageName={'Invoices'}/>
                <Action />
                <Invoices />
            </div>
        )
    }
}
