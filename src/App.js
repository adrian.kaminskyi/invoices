import React from "react"
import { BrowserRouter, Switch, Route } from "react-router-dom"
import Home from "./Pages/Home"
import About from "./Pages/About"

function App() {
  return (
    <BrowserRouter>
      <div className="main">
        <Switch>
          <Route path = {'/'} exact component = {Home}/>
          <Route path = {'/about'} component = {About}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
