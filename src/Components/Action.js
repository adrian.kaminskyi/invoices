import React from 'react'
import { NavLink } from 'react-router-dom'

function Action () {
    return (
        <div className="action-bar">
            <div className="action-bar-wrap">
                <span className="action-bar-name">Action</span>
                <NavLink className="action-bar-btn" to={'/about'}>Add new</NavLink>
            </div>
        </div>
    )
    
}

export default Action;