import React from 'react'

const Header = ({pageName}) => {
    return (
        <header className="title">
            <div className="title-wrap">
                <div className="title-rectangle"></div>
                <span className="title-name">{pageName}</span>
                <div className="title-line" />
            </div>
        </header>
    )
}

export default Header;