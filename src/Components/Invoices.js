import React, { Component } from 'react'

class Invoices extends Component {
    constructor (props) {
        super(props)
        this.state = {
            users: []
        };
    }

    componentWillMount () {
        const url = 'http://localhost:3001/users';
        fetch(url)
        .then(resp => resp.json())
        .then(data => {
            let users = data.map((user, index) => {
                return (
                    <div className="invoice-bar-main-list">
                        <div className="invoice-bar-main-list-item"><span>{user.date_created}</span></div>
                        <div className="invoice-bar-main-list-item"><span>{user.number}</span></div>
                        <div className="invoice-bar-main-list-item"><span>{user.date_supplied}</span></div>
                        <div className="invoice-bar-main-list-item"><span>{user.comment}</span></div>
                    </div>
                )
            })
            this.setState({users: users});
        })
    }

    render () {
        return (
            <div className="invoice-bar">
                <div className="invoice-bar-wrap">
                    <span className="invoice-bar-name">Invoices</span>
                    <div className="invoice-bar-main">
                        <div className="invoice-bar-main-header">
                             <div className="invoice-bar-main-header-item"><span>Create</span></div>
                             <div className="invoice-bar-main-header-item"><span>No</span></div>
                             <div className="invoice-bar-main-header-item"><span>Supply</span></div>
                             <div className="invoice-bar-main-header-item"><span>Comment</span></div>
                        </div>
                        {this.state.users}
                    </div>
                </div>
            </div>
        )
    }
}

export default Invoices;