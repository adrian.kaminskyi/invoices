import Axios from 'axios';
import React, { useState } from 'react'
import { NavLink } from "react-router-dom"

function Create(){
    const url = 'http://localhost:3001/users';
    const [data, setData] = useState({
        number: '',
        date_supplied: '',
        date_created: '',
        comment: '',
        id: '_' + Math.random().toString(36).substr(2, 9)
    })
    
    function handleChange (e) {
        const newData = {...data}
        newData[e.target.id] = e.target.value;
        setData(newData)
        console.log(newData)
    }


    function handleSubmit(e) {
        e.preventDefault()
        Axios.post(url,{
            id: data.id,
            number: data.number,
            date_created: data.date_created,
            date_supplied: data.date_supplied,
            comment: data.comment
        })
        .then(res=> {
            console.log(res.data)
        })
    }

    



    return (
        <div className="create-area">
            <div className="create-area-wrap">
                
                <form onSubmit={(e) => handleSubmit(e)} className="create-area-form">
                    <div className="create-area-form-frame">
                        <div className="frame-input">
                            <div className="frame-input-side">
                                <div className="frame-input-side-label">
                                    <label>Number:</label>
                                    <input onChange={(e) => handleChange(e)} id='number' value={data.number} type="number" minLength="3"/>
                                </div>
                                <div className="frame-input-side-label">
                                    <label>Supply date:</label>
                                    <input onChange={(e) => handleChange(e)} id='date_supplied' value={data.date_supplied} type="date" placeholder="Select Date" minLength="3"/>
                                </div>
                            </div>
                            <div className="frame-input-side">
                                <div className="frame-input-side-label">
                                    <label>Invoice date:</label>
                                    <input onChange={(e) => handleChange(e)} id='date_created' value={data.date_created} type="date" minLength="3"/>
                                </div>
                            </div>
                        </div>

                        <div className="create-area-form-frame-comment">
                            <label>Comment</label>
                            <textarea onChange={(e) => handleChange(e)} id='comment' value={data.comment} maxLength="160"></textarea>
                        </div>
                    </div>

                    <div className="create-area-submit">
                        <button className="create-area-submit-btn" type='submit'><NavLink className="create-area-submit-link" to={'/'}>Save</NavLink></button>
                    </div>
                </form>
                
            </div>
        </div>
    )
    
}

export default Create;

